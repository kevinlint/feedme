import { Feed } from '@core/feeds';

const PROXY_DEV = 'http://localhost:4242';
const PROXY_PROD = 'https://linttrap.deno.dev';
export const PROXY = PROXY_DEV;

export const FEEDS: Omit<Feed, 'articles'>[] = [
  { title: 'Hacker News: Newest', url: 'https://hnrss.org/newest' },
  { title: 'TechCrunch', url: 'https://techcrunch.com/feed/' },
  { title: 'MIT Technology Review', url: 'https://www.technologyreview.com/feed/' },
  { title: 'Wired', url: 'https://www.wired.com/feed/rss' },
  { title: 'Hackaday', url: 'https://hackaday.com/blog/feed/' },
];
