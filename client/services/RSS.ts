import { PROXY } from '@config';

type Article = { title: string; link: string };

export const rss = (url: string) => async (): Promise<Article[]> => {
  let feedString = window.localStorage.getItem(url);

  if (!feedString) {
    const response = await fetch(`${PROXY}?q=${url}`);
    feedString = await response.text();
  }

  const rssDoc = new window.DOMParser().parseFromString(feedString, 'text/xml');

  const articles = Array.from(rssDoc.querySelectorAll('item')).map((i) => ({
    title: (i.querySelector('title').childNodes[0] as CDATASection).data,
    link: i.querySelector('link').innerHTML,
  }));

  return articles;
};
