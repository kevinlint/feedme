export type Article = { title: string; link: string };
const StorageKey = 'articles_read';

export const getArticlesRead = () => {
  const stored = window.localStorage.getItem(StorageKey) ?? '[]';
  const parsed = JSON.parse(stored) as string[];
  return parsed;
};

export const setArticleAsRead = (title: string) => {
  const articlesRead = getArticlesRead();
  articlesRead.push(title);
  window.localStorage.setItem(StorageKey, JSON.stringify(articlesRead));
};
