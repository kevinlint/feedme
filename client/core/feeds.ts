import { FEEDS, PROXY } from '@config';
import { Article } from './articles';

export type Feed = {
  title: string;
  url: string;
  articles: Article[];
};

export const getFeeds = () => {
  const stored = window.localStorage.getItem('feeds') ?? '[]';
  const parsed = JSON.parse(stored) as Feed[];
  return parsed;
};

export const getStoredFeedTitles = () => getFeeds().map((f) => f.title);
export const getFeedsNotStored = () => FEEDS.filter((f) => !getStoredFeedTitles().includes(f.title));

export const parseFeed = (feed: string) => {
  const xml = new window.DOMParser().parseFromString(feed, 'text/xml');
  const title = xml.querySelector('title').innerHTML;
  return {
    title,
    articles: Array.from(xml.querySelectorAll('item')).map((i) => ({
      title: (i.querySelector('title').childNodes[0] as CDATASection).data,
      link: i.querySelector('link').innerHTML,
    })),
  };
};

export const addFeed = async (url: string) => {
  const response = await fetch(`${PROXY}?q=${url}`);
  const data = await response.text();
  if (!data) return false;
  const { title, articles } = parseFeed(data);
  const feeds = getFeeds();
  feeds.push({ title, url, articles });
  window.localStorage.setItem('feeds', JSON.stringify(feeds));
  return true;
};

export const removeFeed = async (index: number) => {
  const feeds = getFeeds().filter((_, i) => i !== index);
  window.localStorage.setItem('feeds', JSON.stringify(feeds));
};
