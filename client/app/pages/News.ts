import { isTheme, ThemePicker } from '@components/ThemePicker';
import { getFeeds, removeFeed } from '@core/feeds';
import { AddForm } from '@features/Feed/AddForm';
import { Articles } from '@features/Feed/Articles';
import { Menu } from '@features/Header';
import { Attr, ClassList, HTML, OnLoad, OnResize, Style } from '@framework/html';
import { Signal } from '@framework/signal';
import { Color, CSS } from '@framework/styles';
import { Logo } from 'client/app/components/Logo';

const css = CSS({
  feed: [
    ['backgroundColor', Color('black', 2), isTheme('DARK')],
    ['backgroundColor', Color('white', 0, 0.2), isTheme('LIGHT')],
    ['padding', '10px'],
    ['borderRadius', '10px'],
    ['minWidth', '300px'],
    ['minHeight', '300px'],
    ['justifyContent', 'center'],
    ['alignItems', 'center'],
    ['display', 'flex'],
  ],
  feeds: [
    ['minHeight', '100vh'],
    ['fontFamily', 'monospace'],
    ['maxWidth', '1800px'],
    ['display', 'flex'],
    ['justifyContent', 'center'],
    ['alignItems', 'flex-start'],
    ['margin', '2vh 0'],
    ['padding', '0 0 0 0'],
    ['transform', 'skewY(-3deg)'],
    ['flex', 1],
    ['height', '100vh'],
    ['overflowY', 'auto'],
    ['overflowX', 'hidden'],
  ],
  grid: [
    ['display', 'grid'],
    ['gridGap', '20px'],
    ['padding', '25px'],
    ['boxSizing', 'border-box'],
    ['color', Color('white')],
    ['paddingBottom', '100px'],
  ],
  one_col: [['gridTemplateColumns', '1fr']],
  row: [
    ['gridColumn', '1/-1'],
    ['display', 'flex'],
    ['alignItems', 'center'],
    ['justifyContent', 'space-between'],
    ['padding', '0 20px'],
    ['position', 'relative'],
  ],
  three_cols: [['gridTemplateColumns', 'repeat(3, 1fr)']],
  ui: [
    ['display', 'flex'],
    ['flexDirection', 'row'],
    ['width', '100vw'],
    ['justifyContent', 'center'],
    ['backgroundColor', Color('black'), isTheme('DARK')],
    ['backgroundColor', Color('white', 0, 0.8), isTheme('LIGHT')],
  ],
});

const NewsViewState = Signal('UPDATED');

const html = HTML({
  css: ClassList,
  style: Style,
  attr: Attr,
  on_view_change: NewsViewState.innerHtml,
  on_resize: OnResize,
  on_load: OnLoad,
});

export const News = () => {
  const handleGridSize = (el: HTMLElement) => {
    const width = window.innerWidth;
    if (width > 0 && width < 1100) el.className = css('grid', 'one_col');
    if (width >= 1100) el.className = css('grid', 'three_cols');
  };

  const getNews = () => {
    const feedList = getFeeds();
    const gridType = css('grid', window.innerWidth < 1100 ? 'one_col' : 'three_cols');
    const grid = html('div', ['on_resize', handleGridSize], ['on_load', handleGridSize], ['css', gridType]);
    const row = html('div', ['css', css('row')]);
    const articles = html('div', ['css', css('feed')]);
    return grid(
      row(ThemePicker(), Logo({ onClick: () => null }), Menu()),
      ...feedList.map((feed, i) => articles(Articles(feed, () => (removeFeed(i), NewsViewState.pub('UPDATED'))))),
      AddForm(() => NewsViewState.pub('UPDATED')),
    );
  };

  const ui = html('div', ['css', css('ui')]);
  const feeds = html('div', ['css', css('feeds')], ['on_view_change', 'UPDATED', getNews]);
  return ui(feeds(getNews()));
};
