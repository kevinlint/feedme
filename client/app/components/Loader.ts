import { ClassList, HTML, Style } from '@framework/html';
import { Color, CSS, KeyFrames } from '@framework/styles';

const keyframes = KeyFrames({
  loading: [
    [0, 'height', '0px'],
    [50, 'height', '20px'],
    [100, 'height', '0px'],
  ],
});

const css = CSS({
  wrapper: [
    ['display', 'flex'],
    ['justifyContent', 'center'],
    ['alignItems', 'center'],
    ['flexDirection', 'column'],
  ],
  bars: [
    ['display', 'flex'],
    ['justifyContent', 'center'],
    ['alignItems', 'center'],
    ['flexDirection', 'row'],
  ],
  bar: [
    ['animation', keyframes('loading')],
    ['animationFillMode', 'forwards'],
    ['animationDuration', '1s'],
    ['animationIterationCount', 'infinite'],
    ['width', '5px'],
    ['height', '0px'],
    ['backgroundColor', Color('white', 0, 0.1)],
    ['margin', '0px 5px'],
    ['borderRadius', '3px'],
  ],
  text: [
    ['fontFamily', 'monospace'],
    ['margin', '20px'],
    ['textTransform', 'uppercase'],
    ['fontSize', '11px'],
    ['letterSpacing', '3px'],
    ['color', Color('white', 0, 0.1)],
    ['fontWeight', 'bold'],
  ],
});

const html = HTML({
  css: ClassList,
  style: Style,
});

export const LoadingAnimation = (text = 'Loading') => {
  const wrapper = html('div', ['css', css('wrapper')]);
  const txt = html('div', ['css', css('text')]);
  const bars = html('div', ['css', css('bars')]);
  const bar1 = html('div', ['css', css('bar')], ['style', 'animation-delay:0s']);
  const bar2 = html('div', ['css', css('bar')], ['style', 'animation-delay:0.1s']);
  const bar3 = html('div', ['css', css('bar')], ['style', 'animation-delay:0.2s']);
  return wrapper(txt(text), bars(bar1(), bar2(), bar3()));
};
