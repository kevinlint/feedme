import { ClassList, HTML, OnClick, OnHoverClassList } from '@framework/html';
import { Color, CSS } from '@framework/styles';
import { isTheme } from './ThemePicker';

const css = CSS({
  bg: [
    ['cursor', 'pointer'],
    ['backgroundColor', Color('white'), isTheme('DARK')],
    ['backgroundColor', Color('black', 0, 0.3), isTheme('LIGHT')],
    ['color', Color('black'), isTheme('DARK')],
    ['color', Color('white'), isTheme('LIGHT')],
    ['padding', '5px 30px'],
    ['borderRadius', '3px'],
    ['transition', 'all 0.25s'],
  ],
  bg_onhover: [
    ['backgroundColor', Color('yellow'), isTheme('DARK')],
    ['backgroundColor', Color('red', 0, 0.4), isTheme('LIGHT')],
    ['color', Color('black'), isTheme('DARK')],
    ['color', Color('white'), isTheme('LIGHT')],
  ],
  text: [
    ['fontFamily', 'monospace'],
    ['fontSize', '25px'],
    ['fontWeight', '900'],
  ],
});

const html = HTML({
  css: ClassList,
  css_on_hover: OnHoverClassList,
  onclick: OnClick,
});

type LogoProps = {
  onClick: () => void;
};

export const Logo = ({ onClick }: LogoProps) => {
  const bg = html('div', ['css', css('bg')], ['onclick', onClick], ['css_on_hover', css('bg', 'bg_onhover')]);
  const txt = html('div', ['css', css('text')]);
  return bg(txt('FEEDME'));
};
