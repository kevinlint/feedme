import { ClassList, ClassListOnHover, HTML, OnClick } from '@framework/html';
import { Model } from '@framework/model';
import { Color, CSS, StylesSignal } from '@framework/styles';

enum Themes {
  'DARK',
  'LIGHT',
}
type ThemeList = keyof typeof Themes;
const storedTheme = window.localStorage.getItem('theme');
export const theme = Model<{ state: ThemeList }>({ state: storedTheme === 'LIGHT' ? 'LIGHT' : 'DARK' });
export const isTheme = (t: ThemeList) => () => theme.get('state') === t;

theme.sub('state', (model) => window.localStorage.setItem('theme', model.state));

const css = CSS({
  wrapper: [
    ['position', 'absolute'],
    ['top', '-25px'],
    ['display', 'flex'],
    ['flexDirection', 'row'],
    ['opacity', 0.3],
    ['cursor', 'pointer'],
  ],
  full_opacity: [
    ['opacity', 1],
    ['transition', 'all 0.5s'],
  ],
  toggle_wrapper: [
    ['display', 'flex'],
    ['flexDirection', 'row'],
    ['borderRadius', '10px'],
    ['border', `2px solid ${Color('white')}`, isTheme('DARK')],
    ['border', `2px solid ${Color('black')}`, isTheme('LIGHT')],
    ['padding', '3px'],
    ['marginRight', '7px'],
    ['width', '30px'],
    ['position', 'relative'],
  ],
  toggle: [
    ['height', '6px'],
    ['borderRadius', '8px'],
    ['backgroundColor', Color('white'), isTheme('DARK')],
    ['backgroundColor', Color('black'), isTheme('LIGHT')],
    ['color', Color('white'), isTheme('DARK')],
    ['color', Color('black'), isTheme('LIGHT')],
    ['padding', '0 6px'],
    ['textTransform', 'uppercase'],
    ['fontWeight', 'bold'],
    ['position', 'absolute'],
    ['transition', 'all 0.25s'],
    ['left', '4px'],
    ['bottom', '3px'],
  ],
  toggle_left: [['left', '4px']],
  toggle_right: [['left', '20px']],
  text: [
    ['fontSize', '9px'],
    ['borderRadius', '8px'],
    ['fontWeight', 'bold'],
    ['lineHeight', '1.8'],
    ['textTransform', 'uppercase'],
    ['fontWeight', 'bold'],
    ['letterSpacing', '1px'],
    ['color', Color('white'), isTheme('DARK')],
    ['color', Color('black'), isTheme('LIGHT')],
  ],
  dark: [
    ['width', '10px'],
    ['height', '15px'],
    ['borderRadius', '3px'],
    ['textTransform', 'uppercase'],
    ['fontWeight', 'bold'],
  ],
});

export const ThemePicker = () => {
  const html = HTML({
    css: ClassList,
    css_on_hover: ClassListOnHover,
    on_click: OnClick,
    on_model_css: theme.css,
    on_model_text: theme.innerText,
  });

  const toggle = () => {
    const state = theme.get('state') === 'LIGHT' ? 'DARK' : 'LIGHT';
    theme.set({ state });
    StylesSignal.pub('RENDER');
  };

  const getToggleCss = () => css('toggle', theme.get('state') === 'DARK' ? 'toggle_left' : 'toggle_right');

  const wrapper = html(
    'div',
    ['css', css('wrapper')],
    ['css_on_hover', css('wrapper', 'full_opacity')],
    ['on_click', toggle],
  );

  const toggleWrapper = html('div', ['css', css('toggle_wrapper')]);
  const toggler = html('div', ['css', getToggleCss()], ['on_model_css', 'state', getToggleCss]);
  const label = html('div', ['css', css('text')], ['on_model_text', 'state']);
  return wrapper(toggleWrapper(toggler()), label(theme.get('state')));
};
