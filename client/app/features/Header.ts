import { isTheme } from '@components/ThemePicker';
import { Attr, ClassList, HTML, Style } from '@framework/html';
import { Color, CSS } from '@framework/styles';

const css = CSS({
  credit: [
    ['color', Color('white', 0, 0.2), isTheme('DARK')],
    ['color', Color('black', 0, 0.3), isTheme('LIGHT')],
    ['margin', '0 10px'],
  ],
  link: [
    ['color', Color('white', 0, 0.5), isTheme('DARK')],
    ['color', Color('black', 0, 0.7), isTheme('LIGHT')],
    ['textDecoration', 'none'],
    ['fontSize', '11px'],
    ['letterSpacing', '2px'],
  ],
  menu: [
    ['display', 'flex'],
    ['flexDirection', 'row'],
    ['alignItems', 'center'],
    ['margin', '0 0 0 10px'],
  ],
});

const html = HTML({
  css: ClassList,
  style: Style,
  attr: Attr,
});

export const Menu = () => {
  const menu = html('div', ['css', css('menu')]);
  const credit = html('div', ['css', css('credit')]);
  const owl = html('div', ['style', 'font-size:20px;margin-right:10px;']);
  const link = html(
    'a',
    ['css', css('link')],
    ['attr', 'href', 'http://github.com/kvnlnt/feedme'],
    ['attr', 'target', '_blank'],
  );
  return menu(credit('Made With Feds'), owl('🦉'), link('GITHUB'));
};
