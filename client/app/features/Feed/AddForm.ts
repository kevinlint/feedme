import { LoadingAnimation } from '@components/Loader';
import { isTheme } from '@components/ThemePicker';
import { addFeed, Feed, getFeedsNotStored } from '@core/feeds';
import { Attr, ClassList, ClassListOnHover, HTML, OnClick, OnTextInput, Style, Value } from '@framework/html';
import { Model } from '@framework/model';
import { Signal } from '@framework/signal';
import { Color, CSS, KeyFrames } from '@framework/styles';

const keyframes = KeyFrames({
  flash: [
    [0, 'backgroundColor', Color('white', 0, 0)],
    [90, 'backgroundColor', Color('white', 0, 0.9)],
    [100, 'backgroundColor', Color('white', 0, 0)],
    [0, 'width', '0%'],
    [10, 'width', '100%'],
    [90, 'width', '100%'],
    [100, 'width', '0%'],
    [0, 'height', '0%'],
    [10, 'height', '100%'],
    [90, 'height', '100%'],
    [100, 'height', '0%'],
    [0, 'borderRadius', '100%'],
    [10, 'borderRadius', '10%'],
  ],
});

const css = CSS({
  added_message: [
    ['fontSize', '12px'],
    ['fontFamily', 'monospace'],
    ['textTransform', 'uppercase'],
    ['letterSpacing', '2px'],
    ['fontWeight', 'bold'],
  ],
  add_feed_button: [
    ['display', 'flex'],
    ['flexDirection', 'column'],
    ['color', Color('white', 0, 0.3), isTheme('DARK')],
    ['color', Color('black', 0, 0.3), isTheme('LIGHT')],
    ['alignItems', 'center'],
    ['justifyContent', 'center'],
    ['height', '100%'],
  ],
  add_feed_button_highlight: [
    ['color', Color('white'), isTheme('DARK')],
    ['color', Color('black'), isTheme('LIGHT')],
    ['transition', 'all 0.25s'],
  ],
  add_feed_text: [
    ['fontSize', '12px'],
    ['fontFamily', 'monospace'],
    ['textTransform', 'uppercase'],
    ['letterSpacing', '2px'],
    ['fontWeight', 'bold'],
  ],
  cancel_button: [
    ['position', 'absolute'],
    ['right', '0'],
    ['top', '0'],
    ['fontSize', '8px'],
    ['fontFamily', 'Arial'],
    ['textTransform', 'uppercase'],
    ['letterSpacing', '2px'],
    ['fontWeight', 'bold'],
    ['color', Color('white', 0, 0.1), isTheme('DARK')],
    ['color', Color('black', 0, 0.1), isTheme('LIGHT')],
  ],
  cancel_button_highlight: [
    ['color', Color('white', 0, 0.5), isTheme('DARK')],
    ['color', Color('black', 0, 0.5), isTheme('LIGHT')],
    ['transition', 'all 0.25s'],
  ],
  container: [
    ['position', 'relative'],
    ['display', 'flex'],
    ['justifyContent', 'center'],
    ['alignItems', 'center'],
    ['minWidth', '300px'],
    ['minHeight', '300px'],
  ],
  feed: [
    ['color', Color('white', 0, 0.5), isTheme('DARK')],
    ['color', Color('black', 0, 0.5), isTheme('LIGHT')],
    ['padding', '5px 10px'],
    ['fontSize', '14px'],
  ],
  feed_highlight: [
    ['color', Color('black'), isTheme('DARK')],
    ['transition', 'all 0.25s'],
    ['backgroundColor', Color('yellow'), isTheme('DARK')],
    ['backgroundColor', Color('yellow'), isTheme('LIGHT')],
  ],
  feeds: [['margin', '10px 0 ']],
  form: [
    ['margin', '15px 0'],
    ['display', 'flex'],
    ['flexDirection', 'row'],
    ['padding', '0px 10px 5px'],
  ],
  form_button: [
    ['margin', '0 10px'],
    ['backgroundColor', Color('white', 0, 0.1), isTheme('DARK')],
    ['backgroundColor', Color('black', 0, 0.1), isTheme('LIGHT')],
    ['color', Color('white', 0, 0.5), isTheme('DARK')],
    ['color', Color('black', 0, 0.5), isTheme('LIGHT')],
    ['padding', '0 20px'],
    ['border', '0px'],
  ],
  form_button_highlight: [
    ['backgroundColor', Color('white', 0, 0.3), isTheme('DARK')],
    ['backgroundColor', Color('black', 0, 0.3), isTheme('LIGHT')],
    ['transition', 'all 0.25s'],
    ['cursor', 'pointer'],
  ],
  form_input: [
    ['backgroundColor', Color('transparent')],
    ['border', `1px solid ${Color('white', 0, 0.1)}`, isTheme('DARK')],
    ['border', `1px solid ${Color('black', 0, 0.1)}`, isTheme('LIGHT')],
    ['padding', '5px'],
    ['color', Color('white', 0, 0.5), isTheme('DARK')],
    ['color', Color('black', 0, 0.5), isTheme('LIGHT')],
    ['flexGrow', 1],
    ['borderRadius', '3px'],
  ],
  form_button_disabled: [
    ['color', Color('white', 0, 0.2), isTheme('DARK')],
    ['color', Color('black', 0, 0.2), isTheme('LIGHT')],
  ],
  form_label: [
    ['fontFamily', 'monospace'],
    ['textTransform', 'uppercase'],
    ['fontSize', '11px'],
    ['letterSpacing', '2px'],
    ['color', Color('white', 0, 0.3), isTheme('DARK')],
    ['color', Color('black', 0, 0.3), isTheme('LIGHT')],
    ['fontWeight', 'bold'],
    ['borderBottom', `1px dashed ${Color('white', 0, 0.1)}`, isTheme('DARK')],
    ['borderBottom', `1px dashed ${Color('black', 0, 0.1)}`, isTheme('LIGHT')],
    ['paddingBottom', '10px'],
  ],
  processing: [
    ['fontSize', '12px'],
    ['fontFamily', 'monospace'],
    ['textTransform', 'uppercase'],
    ['letterSpacing', '2px'],
    ['fontWeight', 'bold'],
  ],
  transition_guard: [
    ['width', '100%'],
    ['height', '100%'],
    ['backgroundColor', Color('white', 0, 1), isTheme('DARK')],
    ['backgroundColor', Color('black', 0, 1), isTheme('LIGHT')],
    ['animation', keyframes('flash')],
    ['animationFillMode', 'forwards'],
    ['animationDuration', '1s'],
    ['animationIterationCount', '1'],
    ['borderRadius', '10px'],
  ],
  transition_guard_wrapper: [
    ['position', 'absolute'],
    ['width', '100%'],
    ['height', '100%'],
    ['zIndex', 1],
    ['pointerEvents', 'none'],
    ['justifyContent', 'center'],
    ['alignItems', 'center'],
    ['display', 'flex'],
  ],
  wrapper: [
    ['backgroundColor', Color('black', 2), isTheme('DARK')],
    ['backgroundColor', Color('white', 0, 0.2), isTheme('LIGHT')],
    ['padding', '20px'],
    ['borderRadius', '10px'],
    ['width', '100%'],
    ['height', '100%'],
    ['justifyContent', 'center'],
    ['alignItems', 'center'],
    ['display', 'flex'],
    ['flexDirection', 'column'],
    ['cursor', 'pointer'],
    ['position', 'absolute'],
    ['boxSizing', 'border-box'],
  ],
  wrapper_content: [
    ['width', '100%'],
    ['height', '100%'],
    ['overflow', 'auto'],
    ['justifyContent', 'flex-start'],
    ['position', 'relative'],
    ['display', 'flex'],
    ['flexDirection', 'column'],
  ],
  wrapper_highlight: [
    ['backgroundColor', Color('black', 3), isTheme('DARK')],
    ['backgroundColor', Color('white', 3), isTheme('LIGHT')],
    ['transition', 'all 0.2s'],
  ],
});

type ControllerState = 'IDLE' | 'ADDING';
type ControllerActions = { type: 'ADD'; url: string } | { type: 'CANCEL' };
type FormParams = { url: string };

export const AddForm = (onupdate: () => void) => {
  const viewState = Signal('ADD_BTN', 'FORM', 'PROCESSING');
  const formModel = Model<FormParams>({ url: '' });
  let controllerState: ControllerState = 'IDLE';

  const html = HTML({
    attr: Attr,
    css: ClassList,
    css_on_hover: ClassListOnHover,
    on_click: OnClick,
    on_input: OnTextInput,
    set_value_on_model_change: formModel.value,
    set_attr_on_model_change: formModel.attr,
    set_css_on_model_change: formModel.css,
    set_html_on_viewstate_change: viewState.innerHtml,
    style: Style,
    value: Value,
  });

  const controller = async (action: ControllerActions) => {
    switch (controllerState) {
      case 'IDLE':
        switch (action.type) {
          case 'ADD':
            controllerState = 'ADDING';
            viewState.pub('PROCESSING');
            await addFeed(action.url);
            setTimeout(() => {
              viewState.pub('ADD_BTN');
              onupdate();
            }, 2000);
        }
      case 'ADDING':
        switch (action.type) {
          case 'CANCEL':
            viewState.pub('ADD_BTN');
            controllerState = 'IDLE';
            break;
        }
        // noop
        break;
    }
  };

  const Processing = () => {
    const wrapper = html('div', ['css', css('wrapper_content')]);
    const processing = html('div', ['css', css('processing')]);
    return wrapper(processing(LoadingAnimation('Processing')));
  };

  const AddBtn = () => {
    const wrapper = html('div', ['css', css('wrapper_content')], ['on_click', () => viewState.pub('FORM')]);
    const btn = html(
      'div',
      ['css', css('add_feed_button')],
      ['css_on_hover', css('add_feed_button', 'add_feed_button_highlight')],
    );
    const plus = html('div', ['css', css('add_feed_text')], ['style', 'font-size:50px;']);
    const txt = html('div', ['css', css('add_feed_text')]);
    return wrapper(btn(plus('+'), txt('Add Feed')));
  };

  const Form = () => {
    const wrapper = html('div', ['css', css('wrapper_content')]);
    const cancel_button = html(
      'div',
      ['css', css('cancel_button')],
      ['css_on_hover', css('cancel_button', 'cancel_button_highlight')],
      ['on_click', () => controller({ type: 'CANCEL' })],
    );
    const label = html('div', ['css', css('form_label')]);
    const form = html('form', ['css', css('form')]);
    const input = html(
      'input',
      ['css', css('form_input')],
      ['on_input', (val) => formModel.set({ url: val })],
      ['set_value_on_model_change', 'url'],
      ['value', formModel.get('url')],
    );
    const add_button = html(
      'button',
      ['attr', 'disabled', 'true'],
      ['css', css('form_button', 'form_button_disabled')],
      ['css_on_hover', css('form_button', 'form_button_highlight')],
      ['on_click', () => controller({ type: 'ADD', url: formModel.get('url') })],
      ['set_attr_on_model_change', 'url', 'disabled', () => (formModel.get('url').length > 0 ? null : 'true')],
      [
        'set_css_on_model_change',
        'url',
        () => (formModel.get('url').length > 0 ? css('form_button') : css('form_button', 'form_button_disabled')),
      ],
    );

    const feed_link = ({ title, url }: Feed) =>
      html(
        'div',
        ['css', css('feed')],
        ['css_on_hover', css('feed', 'feed_highlight')],
        ['on_click', () => formModel.set({ url })],
      )(title);

    return wrapper(
      cancel_button('cancel'),
      label('Add A Feed'),
      form(input(), add_button('+')),
      html('div', ['css', css('form_label')])('Or Pick One From Below'),
      html('div', ['css', css('feeds')])(...getFeedsNotStored().map(feed_link)),
    );
  };

  const wrapper = html(
    'div',
    ['css', css('wrapper')],
    ['set_html_on_viewstate_change', 'ADD_BTN', () => AddBtn()],
    ['set_html_on_viewstate_change', 'FORM', () => Form()],
    ['set_html_on_viewstate_change', 'PROCESSING', () => Processing()],
  );

  return html('div', ['css', css('container')])(wrapper(AddBtn()));
};
