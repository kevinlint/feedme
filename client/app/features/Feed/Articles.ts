import { isTheme } from '@components/ThemePicker';
import { getArticlesRead, setArticleAsRead } from '@core/articles';
import { Feed } from '@core/feeds';
import { Attr, ClassList, ClassListOnHover, HTML, InlineStyles, OnClick } from '@framework/html';
import { Signal } from '@framework/signal';
import { Color, CSS } from '@framework/styles';

const css = CSS({
  delete: [
    ['fontSize', '8px'],
    ['textAlign', 'center'],
    ['color', Color('white', 0, 0.7), isTheme('DARK')],
    ['color', Color('black', 0, 0.7), isTheme('LIGHT')],
    ['position', 'absolute'],
    ['top', 0],
    ['right', 0],
    ['cursor', 'pointer'],
    ['fontFamily', 'arial'],
    ['backgroundColor', Color('black', 0, 1), isTheme('DARK')],
    ['backgroundColor', Color('white', 0, 1), isTheme('LIGHT')],
    ['borderRadius', '100%'],
    ['width', '25px'],
    ['height', '25px'],
    ['display', 'flex'],
    ['alignItems', 'center'],
    ['justifyContent', 'center'],
    ['transition', 'all 0.3s'],
  ],
  delete_hover: [
    ['backgroundColor', Color('white'), isTheme('DARK')],
    ['backgroundColor', Color('black'), isTheme('LIGHT')],
    ['color', Color('black'), isTheme('DARK')],
    ['color', Color('white'), isTheme('LIGHT')],
    ['transition', 'all 0.3s'],
  ],
  feedUrl: [
    ['fontSize', '11px'],
    ['textAlign', 'center'],
    ['textTransform', 'uppercase'],
    ['color', Color('white', 0, 0.1), isTheme('DARK')],
    ['color', Color('black', 0, 0.2), isTheme('LIGHT')],
    ['marginBottom', '10px'],
    ['wordBreak', 'break-all'],
  ],
  link: [
    ['display', 'block'],
    ['textAlign', 'center'],
    ['padding', '5px'],
    ['fontSize', '12px'],
    ['padding', '5px'],
    ['transition', 'all 0.5s'],
    ['borderRadius', '3px'],
  ],
  link_unread: [
    ['color', Color('white', 0, 0.5), isTheme('DARK')],
    ['color', Color('black', 0, 0.5), isTheme('LIGHT')],
    ['textDecoration', 'none'],
  ],
  link_read: [
    ['textDecoration', 'line-through'],
    ['color', Color('white', 0, 0.1), isTheme('DARK')],
    ['color', Color('black', 0, 0.1), isTheme('LIGHT')],
  ],
  link_hover: [
    ['color', Color('black'), isTheme('DARK')],
    ['backgroundColor', Color('yellow'), isTheme('DARK')],
    ['backgroundColor', Color('red', 0, 0.4), isTheme('LIGHT')],
  ],
  title: [
    ['fontSize', '15px'],
    ['textAlign', 'center'],
    ['marginBottom', '5px'],
    ['textIndent', '5px'],
    ['padding', '10px 0 5px'],
    ['color', Color('white', 0, 0.7), isTheme('DARK')],
    ['color', Color('black', 0, 0.7), isTheme('LIGHT')],
  ],
});

const ArticleSignal = Signal('UPDATED');

const html = HTML({
  css: ClassList,
  css_on_hover: ClassListOnHover,
  attr: Attr,
  on_click: OnClick,
  on_signal: ArticleSignal.innerHtml,
  style: InlineStyles,
});

export const Articles = ({ title, url, articles }: Feed, ondelete: () => void) => {
  const ArticleList = () => {
    const wrapper = html('div', [
      'style',
      ['paddingLeft', '20px'],
      ['paddingRight', '20px'],
      ['marginBottom', '20px'],
      ['height', '300px'],
      ['overflow', 'auto'],
    ]);
    return wrapper(
      ...articles.map(({ title, link }) =>
        html(
          'a',
          ['css', getArticlesRead().includes(title) ? css('link', 'link_read') : css('link', 'link_unread')],
          ['css_on_hover', css('link', 'link_hover')],
          ['attr', 'href', link],
          ['attr', 'target', '_blank'],
          ['on_click', () => (setArticleAsRead(title), ArticleSignal.pub('UPDATED'))],
        )(title),
      ),
    );
  };

  const wrapper = html('div', ['style', ['width', '100%'], ['position', 'relative']]);
  const title_wrapper = html('div', ['css', css('title')]);
  const delete_button = html(
    'div',
    ['css', css('delete')],
    ['on_click', ondelete],
    ['css_on_hover', css('delete', 'delete_hover')],
  );
  const feed_url = html('div', ['css', css('feedUrl')]);
  const list = html('div', ['on_signal', 'UPDATED', ArticleList]);
  return wrapper(title_wrapper(title), delete_button('X'), feed_url(url), list(ArticleList()));
};
