import { Signal } from './signal';

const Colors = {
  black: [0, 0, 5],
  brown: [44, 11, 14],
  red: [0, 61, 40],
  blue: [240, 100, 50],
  yellow: [55, 100, 50],
  green: [118, 100, 50],
  purple: [270, 100, 50],
  orange: [30, 100, 50],
  transparent: 'transparent',
  white: [0, 0, 100],
};

/**
 * Scope classes
 * @param str
 * @returns
 */
function uuid(str: string = 'xxxxxxxx') {
  function getRandomSymbol(symbol: string) {
    let array;
    if (symbol === 'y') {
      array = ['8', '9', 'a', 'b'];
      return array[Math.floor(Math.random() * array.length)];
    }
    array = new Uint8Array(1);
    window.crypto.getRandomValues(array);
    return (array[0] % 16).toString(16);
  }
  return str.replace(/[xy]/g, getRandomSymbol);
}

// Expose signal for rerendering
export const StylesSignal = Signal('RENDER');

/**
 * Colorizer function
 * @param color
 * @param adjustLightness
 * @param opacity
 * @returns
 */
export const Color = (color: keyof typeof Colors, adjustLightness: number = 0, opacity: number = 1) => {
  if (color === 'transparent') return color;
  const [h, s, l] = Colors[color];
  const hsla = `hsla(${h}deg,${s}%,${l + adjustLightness}%,${opacity})`;
  return hsla;
};

// Types

type CssPropType = keyof CSSStyleDeclaration;
type CssPropValType = string | number;
type ShouldRender = () => boolean;
type StyleDeclaration = [CssPropType, CssPropValType, ShouldRender?];

// CSS

export const CSS = <T>(declarations: Record<keyof T, StyleDeclaration[]>): ((...list: (keyof T)[]) => string) => {
  const id = uuid();
  const style = document.createElement('style');
  style.id = id;
  document.getElementsByTagName('head')[0].appendChild(style);
  const styles: string[] = [];

  const render = () => {
    style.innerHTML = '';
    Object.entries(declarations).forEach(([selector, styleDeclarations]: [string, StyleDeclaration[]]) => {
      styles.push(`.${selector}_${id} {`);
      styleDeclarations.forEach(([prop, val, render]) => {
        const shouldRender = render ? render() : true;
        return shouldRender ? styles.push(`${(<string>prop).replace(/([A-Z])/g, '-$1').toLowerCase()}:${val};`) : null;
      });
      styles.push(`}\n`);
    });
    style.innerHTML = styles.join('');
  };

  render();
  StylesSignal.sub('RENDER', render);

  const getter = (...list: (keyof T)[]) => {
    return list
      .filter((i) => i !== null)
      .map((item) => `${item}_${id}`)
      .join(' ');
  };

  return getter;
};

// Keyframes

type KeyframeStyleDeclaration = [number, CssPropType, CssPropValType][];
export const KeyFrames = <T>(
  declarations: Record<keyof T, KeyframeStyleDeclaration>,
): ((...list: (keyof T)[]) => string) => {
  const id = uuid();
  const style = document.createElement('style');
  style.id = id;
  document.getElementsByTagName('head')[0].appendChild(style);

  const render = () => {
    style.innerHTML = '';
    const styles: string[] = [];
    const declarationList = Object.entries(declarations).sort((a, b) => (a[0] < b[0] ? -1 : 1));
    declarationList.forEach(([selector, declaration]: [string, KeyframeStyleDeclaration]) => {
      styles.push(`@keyframes ${selector}_${id} {\n`);
      declaration.forEach(([percent, prop, val]) =>
        styles.push(`${percent}% { ${(<string>prop).replace(/([A-Z])/g, '-$1').toLowerCase()}: ${val}; }\n`),
      );
      styles.push(`}\n`);
    });
    style.innerHTML = styles.join('');
  };

  render();

  const getter = (...list: (keyof T)[]) => list.map((item) => `${item}_${id}`).join(', ');

  return getter;
};
