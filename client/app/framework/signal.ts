/** Signal
 * A signal is a simple event emitter.
 */
export const Signal = <T extends string[]>(...signals: T) => {
  const subscribers: { eventName: string; cb: Function }[] = [];
  const pub = (eventName: T[number]) => subscribers.filter((i) => i.eventName === eventName).forEach((i) => i.cb());
  const sub = (eventName: T[number], cb: Function) => subscribers.push({ eventName, cb });
  const innerHtml = (el: HTMLElement, event: T[number], cb: () => any) =>
    sub(event, () => {
      el.innerHTML = '';
      const content = cb();
      if (content) el.appendChild(content);
    });
  const innerText = (el: HTMLElement, event: T[number], cb: () => string) => sub(event, () => (el.innerText = cb()));
  return { pub, sub, innerHtml, innerText };
};
