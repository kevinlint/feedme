export const Model = <T>(
  model: T,
): {
  get: (key: keyof T) => T[keyof T];
  set: (update: Partial<T>) => T;
  innerText: (el: HTMLElement, key: keyof T) => void;
  css: (el: HTMLElement, key: keyof T, cb: () => string) => number;
  attr: (el: HTMLElement, key: keyof T, attr: 'disabled', cb: () => string) => void;
  value: (el: HTMLElement, key: keyof T) => void;
  sub: (key: keyof T, cb: (model: T) => void) => void;
} => {
  const subscribers: { key: keyof T; cb: Function }[] = [];

  let _model: T = model;

  const get = (key: keyof T) => _model[key];

  const set = (update: Partial<T>) => {
    _model = { ..._model, ...update };
    subscribers.filter((i) => Object.keys(_model).includes(String(i.key))).forEach((i) => i.cb(_model));
    return _model;
  };

  const innerText = (el: HTMLElement, key: keyof T) =>
    subscribers.push({ key, cb: () => (el.innerText = String(_model[key])) });

  const css = (el: HTMLElement, key: keyof T, cb: () => string) =>
    subscribers.push({ key, cb: () => (el.className = cb()) });

  const value = (el: HTMLInputElement, key: keyof T) =>
    subscribers.push({ key, cb: () => (el.value = String(_model[key])) });

  const attr = (el: HTMLElement, key: keyof T, attr: 'disabled', cb: () => string) =>
    subscribers.push({ key, cb: () => (cb() === null ? el.removeAttribute(attr) : el.setAttribute(attr, cb())) });

  const sub = (key: keyof T, cb: (model: T) => void) => {
    subscribers.push({ key, cb });
  };

  return {
    get,
    set,
    innerText,
    css,
    value,
    attr,
    sub,
  };
};
