// Typings
type OmitFirstArg<T> = T extends (x: any, ...args: infer P) => infer R ? (...args: P) => R : never;
type Tags = keyof HTMLElementTagNameMap;
type FeatureProp = string;
type FeatureFunc = (el: HTMLElement, ...a: any) => any;
type FeatureParams<T> = Parameters<OmitFirstArg<T>>;

/** HTML
 * A simple template engine.
 */
export const HTML =
  <T extends Record<FeatureProp, FeatureFunc>>(features: T) =>
  <KS extends Array<keyof T>>(
    tag: Tags,
    ...attrs: { [I in keyof KS]-?: [KS[I], ...FeatureParams<T[Extract<KS[I], keyof T>]>] }
  ) =>
  (...children: (HTMLElement | string | number | SVGElement)[]) => {
    // Create element
    const el = document.createElementNS('http://www.w3.org/1999/xhtml', tag);

    // Run features
    attrs.forEach(([attr, ...args]) => features[attr](el, ...args));

    // Append children
    children.forEach((child) => {
      if (child instanceof Node) el.appendChild(child);
      if (typeof child === 'string' || typeof child === 'number') el.innerHTML += child;
    });

    return el;
  };

// Features

/** Attr
 * Set an attribute on an element.
 */
export const Attr = (el: HTMLElement, prop: 'id' | 'href' | 'target' | 'disabled', val: string) =>
  el.setAttribute(prop, String(val));

/**
 * ClassList
 * Add a class to an element.
 * @param el the element to set the class on
 * @param classes the classes to set
 * @returns void
 */
export const ClassList = (el: HTMLElement, classes: string) => (el.className = classes);

export const ClassListOnHover = (el: HTMLElement, classes: string) => {
  const originalClasses = el.className;
  el.addEventListener('mouseover', () => (el.className = classes));
  el.addEventListener('mouseout', () => (el.className = originalClasses));
};

/**
 * OnClick
 * Add a click event listener to an element.
 * @param el The element to add the event listener to.
 * @param cb The callback to run when the event is triggered.
 * @returns void
 */
export const OnClick = (el: HTMLElement, cb: (e: MouseEvent) => void) => el.addEventListener('click', cb);

/**
 * OnHover
 * Add a click event listener to an element.
 * @param el The element to add the event listener to.
 * @param cb The callback to run when the event is triggered.
 * @returns void
 */
export const OnHoverClassList = (el: HTMLElement, classes: string) => {
  const originalClasses = el.className;
  el.addEventListener('mouseover', () => (el.className = classes));
  el.addEventListener('mouseout', () => (el.className = originalClasses));
};

/**
 * OnLoad
 * Add a load event listener to an element.
 * @param el The element to add the event listener to.
 * @param cb The callback to run when the event is triggered.
 * @returns void
 */
export const OnLoad = (el: HTMLElement, cb: (el: HTMLElement) => void) => window.addEventListener('load', () => cb(el));

/**
 * OnResize
 * Add a resize event listener to an element.
 * @param el The element to add the event listener to.
 * @param cb The callback to run when the event is triggered.
 * @returns void
 */
export const OnResize = (el: HTMLElement, cb: (el: HTMLElement) => void) =>
  window.addEventListener('resize', () => cb(el));

/**
 * OnTextInput
 * Add a text input event listener to an element.
 * @param el The element to add the event listener to.
 * @param cb The callback to run when the event is triggered.
 * @returns void
 */
export const OnTextInput = (el: HTMLInputElement, cb: (val: string) => void) =>
  el.addEventListener('input', () => cb(el.value));

/**
 * Style
 * Set a style on an element.
 * @param el The element to set the style on.
 * @param style The style to set.
 * @returns void
 */
export const Style = (el: HTMLElement, style: string) => el.setAttribute('style', style);

/**
 * InlineStyles
 * @param el
 * @param styles
 * @returns
 */
export const InlineStyles = (el: HTMLElement, ...styles: [keyof CSSStyleDeclaration, string | number][]) =>
  el.setAttribute(
    'style',
    styles.map(([prop, val]) => `${(<string>prop).replace(/([A-Z])/g, '-$1').toLowerCase()}:${val};`).join(''),
  );

/**
 * Value
 * Add value to input element
 * @param el the element to set the class on
 * @param classes the classes to set
 * @returns void
 */
export const Value = (el: HTMLInputElement, value: string) => (el.value = value);
