import { theme } from '@components/ThemePicker';
import { HTML } from '@framework/html';
import { Color } from '@framework/styles';
import { News } from '@pages/News';

const html = HTML({});

window.addEventListener('DOMContentLoaded', async () => {
  const getStyles = () => `
    html,body {
      background-color: rgb(13, 13, 13);
      padding: 0px;
      margin: 0px;
      display: flex;
      justify-content: center;
      align-items: flex-start;
      width: 100%;
      height: 100%;
      overflow:hidden;
    }
    *::-webkit-scrollbar { width: 12px; }
    *::-webkit-scrollbar-thumb {
      border-radius: 20px; 
      border: 1px solid ${Color('white', 0, theme.get('state') === 'DARK' ? 0.1 : 0.6)}; 
    }
  `;

  const globalStyles = html('style')(getStyles());
  theme.sub('state', (model) => (globalStyles.innerHTML = getStyles()));
  document.head.appendChild(globalStyles);
  document.body.appendChild(News());
});
