(() => {
  // client/app/framework/html.ts
  var HTML = (features) => (tag, ...attrs) => (...children) => {
    const el = document.createElementNS("http://www.w3.org/1999/xhtml", tag);
    attrs.forEach(([attr, ...args]) => features[attr](el, ...args));
    children.forEach((child) => {
      if (child instanceof Node)
        el.appendChild(child);
      if (typeof child === "string" || typeof child === "number")
        el.innerHTML += child;
    });
    return el;
  };
  var Attr = (el, prop, val) => el.setAttribute(prop, String(val));
  var ClassList = (el, classes) => el.className = classes;
  var ClassListOnHover = (el, classes) => {
    const originalClasses = el.className;
    el.addEventListener("mouseover", () => el.className = classes);
    el.addEventListener("mouseout", () => el.className = originalClasses);
  };
  var OnClick = (el, cb) => el.addEventListener("click", cb);
  var OnHoverClassList = (el, classes) => {
    const originalClasses = el.className;
    el.addEventListener("mouseover", () => el.className = classes);
    el.addEventListener("mouseout", () => el.className = originalClasses);
  };
  var OnLoad = (el, cb) => window.addEventListener("load", () => cb(el));
  var OnResize = (el, cb) => window.addEventListener("resize", () => cb(el));
  var OnTextInput = (el, cb) => el.addEventListener("input", () => cb(el.value));
  var Style = (el, style) => el.setAttribute("style", style);
  var InlineStyles = (el, ...styles) => el.setAttribute("style", styles.map(([prop, val]) => `${prop.replace(/([A-Z])/g, "-$1").toLowerCase()}:${val};`).join(""));
  var Value = (el, value) => el.value = value;

  // client/app/framework/model.ts
  var Model = (model) => {
    const subscribers = [];
    let _model = model;
    const get = (key) => _model[key];
    const set = (update) => {
      _model = { ..._model, ...update };
      subscribers.filter((i) => Object.keys(_model).includes(String(i.key))).forEach((i) => i.cb(_model));
      return _model;
    };
    const innerText = (el, key) => subscribers.push({ key, cb: () => el.innerText = String(_model[key]) });
    const css8 = (el, key, cb) => subscribers.push({ key, cb: () => el.className = cb() });
    const value = (el, key) => subscribers.push({ key, cb: () => el.value = String(_model[key]) });
    const attr = (el, key, attr2, cb) => subscribers.push({ key, cb: () => cb() === null ? el.removeAttribute(attr2) : el.setAttribute(attr2, cb()) });
    const sub = (key, cb) => {
      subscribers.push({ key, cb });
    };
    return {
      get,
      set,
      innerText,
      css: css8,
      value,
      attr,
      sub
    };
  };

  // client/app/framework/signal.ts
  var Signal = (...signals) => {
    const subscribers = [];
    const pub = (eventName) => subscribers.filter((i) => i.eventName === eventName).forEach((i) => i.cb());
    const sub = (eventName, cb) => subscribers.push({ eventName, cb });
    const innerHtml = (el, event, cb) => sub(event, () => {
      el.innerHTML = "";
      const content = cb();
      if (content)
        el.appendChild(content);
    });
    const innerText = (el, event, cb) => sub(event, () => el.innerText = cb());
    return { pub, sub, innerHtml, innerText };
  };

  // client/app/framework/styles.ts
  var Colors = {
    black: [0, 0, 5],
    brown: [44, 11, 14],
    red: [0, 61, 40],
    blue: [240, 100, 50],
    yellow: [55, 100, 50],
    green: [118, 100, 50],
    purple: [270, 100, 50],
    orange: [30, 100, 50],
    transparent: "transparent",
    white: [0, 0, 100]
  };
  function uuid(str = "xxxxxxxx") {
    function getRandomSymbol(symbol) {
      let array;
      if (symbol === "y") {
        array = ["8", "9", "a", "b"];
        return array[Math.floor(Math.random() * array.length)];
      }
      array = new Uint8Array(1);
      window.crypto.getRandomValues(array);
      return (array[0] % 16).toString(16);
    }
    return str.replace(/[xy]/g, getRandomSymbol);
  }
  var StylesSignal = Signal("RENDER");
  var Color = (color, adjustLightness = 0, opacity = 1) => {
    if (color === "transparent")
      return color;
    const [h, s, l] = Colors[color];
    const hsla = `hsla(${h}deg,${s}%,${l + adjustLightness}%,${opacity})`;
    return hsla;
  };
  var CSS = (declarations) => {
    const id = uuid();
    const style = document.createElement("style");
    style.id = id;
    document.getElementsByTagName("head")[0].appendChild(style);
    const styles = [];
    const render = () => {
      style.innerHTML = "";
      Object.entries(declarations).forEach(([selector, styleDeclarations]) => {
        styles.push(`.${selector}_${id} {`);
        styleDeclarations.forEach(([prop, val, render2]) => {
          const shouldRender = render2 ? render2() : true;
          return shouldRender ? styles.push(`${prop.replace(/([A-Z])/g, "-$1").toLowerCase()}:${val};`) : null;
        });
        styles.push(`}
`);
      });
      style.innerHTML = styles.join("");
    };
    render();
    StylesSignal.sub("RENDER", render);
    const getter = (...list) => {
      return list.filter((i) => i !== null).map((item) => `${item}_${id}`).join(" ");
    };
    return getter;
  };
  var KeyFrames = (declarations) => {
    const id = uuid();
    const style = document.createElement("style");
    style.id = id;
    document.getElementsByTagName("head")[0].appendChild(style);
    const render = () => {
      style.innerHTML = "";
      const styles = [];
      const declarationList = Object.entries(declarations).sort((a, b) => a[0] < b[0] ? -1 : 1);
      declarationList.forEach(([selector, declaration]) => {
        styles.push(`@keyframes ${selector}_${id} {
`);
        declaration.forEach(([percent, prop, val]) => styles.push(`${percent}% { ${prop.replace(/([A-Z])/g, "-$1").toLowerCase()}: ${val}; }
`));
        styles.push(`}
`);
      });
      style.innerHTML = styles.join("");
    };
    render();
    const getter = (...list) => list.map((item) => `${item}_${id}`).join(", ");
    return getter;
  };

  // client/app/components/ThemePicker.ts
  var storedTheme = window.localStorage.getItem("theme");
  var theme = Model({ state: storedTheme === "LIGHT" ? "LIGHT" : "DARK" });
  var isTheme = (t) => () => theme.get("state") === t;
  theme.sub("state", (model) => window.localStorage.setItem("theme", model.state));
  var css = CSS({
    wrapper: [
      ["position", "absolute"],
      ["top", "-25px"],
      ["display", "flex"],
      ["flexDirection", "row"],
      ["opacity", 0.3],
      ["cursor", "pointer"]
    ],
    full_opacity: [
      ["opacity", 1],
      ["transition", "all 0.5s"]
    ],
    toggle_wrapper: [
      ["display", "flex"],
      ["flexDirection", "row"],
      ["borderRadius", "10px"],
      ["border", `2px solid ${Color("white")}`, isTheme("DARK")],
      ["border", `2px solid ${Color("black")}`, isTheme("LIGHT")],
      ["padding", "3px"],
      ["marginRight", "7px"],
      ["width", "30px"],
      ["position", "relative"]
    ],
    toggle: [
      ["height", "6px"],
      ["borderRadius", "8px"],
      ["backgroundColor", Color("white"), isTheme("DARK")],
      ["backgroundColor", Color("black"), isTheme("LIGHT")],
      ["color", Color("white"), isTheme("DARK")],
      ["color", Color("black"), isTheme("LIGHT")],
      ["padding", "0 6px"],
      ["textTransform", "uppercase"],
      ["fontWeight", "bold"],
      ["position", "absolute"],
      ["transition", "all 0.25s"],
      ["left", "4px"],
      ["bottom", "3px"]
    ],
    toggle_left: [["left", "4px"]],
    toggle_right: [["left", "20px"]],
    text: [
      ["fontSize", "9px"],
      ["borderRadius", "8px"],
      ["fontWeight", "bold"],
      ["lineHeight", "1.8"],
      ["textTransform", "uppercase"],
      ["fontWeight", "bold"],
      ["letterSpacing", "1px"],
      ["color", Color("white"), isTheme("DARK")],
      ["color", Color("black"), isTheme("LIGHT")]
    ],
    dark: [
      ["width", "10px"],
      ["height", "15px"],
      ["borderRadius", "3px"],
      ["textTransform", "uppercase"],
      ["fontWeight", "bold"]
    ]
  });
  var ThemePicker = () => {
    const html7 = HTML({
      css: ClassList,
      css_on_hover: ClassListOnHover,
      on_click: OnClick,
      on_model_css: theme.css,
      on_model_text: theme.innerText
    });
    const toggle = () => {
      const state = theme.get("state") === "LIGHT" ? "DARK" : "LIGHT";
      theme.set({ state });
      StylesSignal.pub("RENDER");
    };
    const getToggleCss = () => css("toggle", theme.get("state") === "DARK" ? "toggle_left" : "toggle_right");
    const wrapper = html7("div", ["css", css("wrapper")], ["css_on_hover", css("wrapper", "full_opacity")], ["on_click", toggle]);
    const toggleWrapper = html7("div", ["css", css("toggle_wrapper")]);
    const toggler = html7("div", ["css", getToggleCss()], ["on_model_css", "state", getToggleCss]);
    const label = html7("div", ["css", css("text")], ["on_model_text", "state"]);
    return wrapper(toggleWrapper(toggler()), label(theme.get("state")));
  };

  // client/config.ts
  var PROXY_DEV = "http://localhost:4242";
  var PROXY = PROXY_DEV;
  var FEEDS = [
    { title: "Hacker News: Newest", url: "https://hnrss.org/newest" },
    { title: "TechCrunch", url: "https://techcrunch.com/feed/" },
    { title: "MIT Technology Review", url: "https://www.technologyreview.com/feed/" },
    { title: "Wired", url: "https://www.wired.com/feed/rss" },
    { title: "Hackaday", url: "https://hackaday.com/blog/feed/" }
  ];

  // client/core/feeds.ts
  var getFeeds = () => {
    const stored = window.localStorage.getItem("feeds") ?? "[]";
    const parsed = JSON.parse(stored);
    return parsed;
  };
  var getStoredFeedTitles = () => getFeeds().map((f) => f.title);
  var getFeedsNotStored = () => FEEDS.filter((f) => !getStoredFeedTitles().includes(f.title));
  var parseFeed = (feed) => {
    const xml = new window.DOMParser().parseFromString(feed, "text/xml");
    const title = xml.querySelector("title").innerHTML;
    return {
      title,
      articles: Array.from(xml.querySelectorAll("item")).map((i) => ({
        title: i.querySelector("title").childNodes[0].data,
        link: i.querySelector("link").innerHTML
      }))
    };
  };
  var addFeed = async (url) => {
    const response = await fetch(`${PROXY}?q=${url}`);
    const data = await response.text();
    if (!data)
      return false;
    const { title, articles } = parseFeed(data);
    const feeds = getFeeds();
    feeds.push({ title, url, articles });
    window.localStorage.setItem("feeds", JSON.stringify(feeds));
    return true;
  };
  var removeFeed = async (index) => {
    const feeds = getFeeds().filter((_, i) => i !== index);
    window.localStorage.setItem("feeds", JSON.stringify(feeds));
  };

  // client/app/components/Loader.ts
  var keyframes = KeyFrames({
    loading: [
      [0, "height", "0px"],
      [50, "height", "20px"],
      [100, "height", "0px"]
    ]
  });
  var css2 = CSS({
    wrapper: [
      ["display", "flex"],
      ["justifyContent", "center"],
      ["alignItems", "center"],
      ["flexDirection", "column"]
    ],
    bars: [
      ["display", "flex"],
      ["justifyContent", "center"],
      ["alignItems", "center"],
      ["flexDirection", "row"]
    ],
    bar: [
      ["animation", keyframes("loading")],
      ["animationFillMode", "forwards"],
      ["animationDuration", "1s"],
      ["animationIterationCount", "infinite"],
      ["width", "5px"],
      ["height", "0px"],
      ["backgroundColor", Color("white", 0, 0.1)],
      ["margin", "0px 5px"],
      ["borderRadius", "3px"]
    ],
    text: [
      ["fontFamily", "monospace"],
      ["margin", "20px"],
      ["textTransform", "uppercase"],
      ["fontSize", "11px"],
      ["letterSpacing", "3px"],
      ["color", Color("white", 0, 0.1)],
      ["fontWeight", "bold"]
    ]
  });
  var html = HTML({
    css: ClassList,
    style: Style
  });
  var LoadingAnimation = (text = "Loading") => {
    const wrapper = html("div", ["css", css2("wrapper")]);
    const txt = html("div", ["css", css2("text")]);
    const bars = html("div", ["css", css2("bars")]);
    const bar1 = html("div", ["css", css2("bar")], ["style", "animation-delay:0s"]);
    const bar2 = html("div", ["css", css2("bar")], ["style", "animation-delay:0.1s"]);
    const bar3 = html("div", ["css", css2("bar")], ["style", "animation-delay:0.2s"]);
    return wrapper(txt(text), bars(bar1(), bar2(), bar3()));
  };

  // client/app/features/Feed/AddForm.ts
  var keyframes2 = KeyFrames({
    flash: [
      [0, "backgroundColor", Color("white", 0, 0)],
      [90, "backgroundColor", Color("white", 0, 0.9)],
      [100, "backgroundColor", Color("white", 0, 0)],
      [0, "width", "0%"],
      [10, "width", "100%"],
      [90, "width", "100%"],
      [100, "width", "0%"],
      [0, "height", "0%"],
      [10, "height", "100%"],
      [90, "height", "100%"],
      [100, "height", "0%"],
      [0, "borderRadius", "100%"],
      [10, "borderRadius", "10%"]
    ]
  });
  var css3 = CSS({
    added_message: [
      ["fontSize", "12px"],
      ["fontFamily", "monospace"],
      ["textTransform", "uppercase"],
      ["letterSpacing", "2px"],
      ["fontWeight", "bold"]
    ],
    add_feed_button: [
      ["display", "flex"],
      ["flexDirection", "column"],
      ["color", Color("white", 0, 0.3), isTheme("DARK")],
      ["color", Color("black", 0, 0.3), isTheme("LIGHT")],
      ["alignItems", "center"],
      ["justifyContent", "center"],
      ["height", "100%"]
    ],
    add_feed_button_highlight: [
      ["color", Color("white"), isTheme("DARK")],
      ["color", Color("black"), isTheme("LIGHT")],
      ["transition", "all 0.25s"]
    ],
    add_feed_text: [
      ["fontSize", "12px"],
      ["fontFamily", "monospace"],
      ["textTransform", "uppercase"],
      ["letterSpacing", "2px"],
      ["fontWeight", "bold"]
    ],
    cancel_button: [
      ["position", "absolute"],
      ["right", "0"],
      ["top", "0"],
      ["fontSize", "8px"],
      ["fontFamily", "Arial"],
      ["textTransform", "uppercase"],
      ["letterSpacing", "2px"],
      ["fontWeight", "bold"],
      ["color", Color("white", 0, 0.1), isTheme("DARK")],
      ["color", Color("black", 0, 0.1), isTheme("LIGHT")]
    ],
    cancel_button_highlight: [
      ["color", Color("white", 0, 0.5), isTheme("DARK")],
      ["color", Color("black", 0, 0.5), isTheme("LIGHT")],
      ["transition", "all 0.25s"]
    ],
    container: [
      ["position", "relative"],
      ["display", "flex"],
      ["justifyContent", "center"],
      ["alignItems", "center"],
      ["minWidth", "300px"],
      ["minHeight", "300px"]
    ],
    feed: [
      ["color", Color("white", 0, 0.5), isTheme("DARK")],
      ["color", Color("black", 0, 0.5), isTheme("LIGHT")],
      ["padding", "5px 10px"],
      ["fontSize", "14px"]
    ],
    feed_highlight: [
      ["color", Color("black"), isTheme("DARK")],
      ["transition", "all 0.25s"],
      ["backgroundColor", Color("yellow"), isTheme("DARK")],
      ["backgroundColor", Color("yellow"), isTheme("LIGHT")]
    ],
    feeds: [["margin", "10px 0 "]],
    form: [
      ["margin", "15px 0"],
      ["display", "flex"],
      ["flexDirection", "row"],
      ["padding", "0px 10px 5px"]
    ],
    form_button: [
      ["margin", "0 10px"],
      ["backgroundColor", Color("white", 0, 0.1), isTheme("DARK")],
      ["backgroundColor", Color("black", 0, 0.1), isTheme("LIGHT")],
      ["color", Color("white", 0, 0.5), isTheme("DARK")],
      ["color", Color("black", 0, 0.5), isTheme("LIGHT")],
      ["padding", "0 20px"],
      ["border", "0px"]
    ],
    form_button_highlight: [
      ["backgroundColor", Color("white", 0, 0.3), isTheme("DARK")],
      ["backgroundColor", Color("black", 0, 0.3), isTheme("LIGHT")],
      ["transition", "all 0.25s"],
      ["cursor", "pointer"]
    ],
    form_input: [
      ["backgroundColor", Color("transparent")],
      ["border", `1px solid ${Color("white", 0, 0.1)}`, isTheme("DARK")],
      ["border", `1px solid ${Color("black", 0, 0.1)}`, isTheme("LIGHT")],
      ["padding", "5px"],
      ["color", Color("white", 0, 0.5), isTheme("DARK")],
      ["color", Color("black", 0, 0.5), isTheme("LIGHT")],
      ["flexGrow", 1],
      ["borderRadius", "3px"]
    ],
    form_button_disabled: [
      ["color", Color("white", 0, 0.2), isTheme("DARK")],
      ["color", Color("black", 0, 0.2), isTheme("LIGHT")]
    ],
    form_label: [
      ["fontFamily", "monospace"],
      ["textTransform", "uppercase"],
      ["fontSize", "11px"],
      ["letterSpacing", "2px"],
      ["color", Color("white", 0, 0.3), isTheme("DARK")],
      ["color", Color("black", 0, 0.3), isTheme("LIGHT")],
      ["fontWeight", "bold"],
      ["borderBottom", `1px dashed ${Color("white", 0, 0.1)}`, isTheme("DARK")],
      ["borderBottom", `1px dashed ${Color("black", 0, 0.1)}`, isTheme("LIGHT")],
      ["paddingBottom", "10px"]
    ],
    processing: [
      ["fontSize", "12px"],
      ["fontFamily", "monospace"],
      ["textTransform", "uppercase"],
      ["letterSpacing", "2px"],
      ["fontWeight", "bold"]
    ],
    transition_guard: [
      ["width", "100%"],
      ["height", "100%"],
      ["backgroundColor", Color("white", 0, 1), isTheme("DARK")],
      ["backgroundColor", Color("black", 0, 1), isTheme("LIGHT")],
      ["animation", keyframes2("flash")],
      ["animationFillMode", "forwards"],
      ["animationDuration", "1s"],
      ["animationIterationCount", "1"],
      ["borderRadius", "10px"]
    ],
    transition_guard_wrapper: [
      ["position", "absolute"],
      ["width", "100%"],
      ["height", "100%"],
      ["zIndex", 1],
      ["pointerEvents", "none"],
      ["justifyContent", "center"],
      ["alignItems", "center"],
      ["display", "flex"]
    ],
    wrapper: [
      ["backgroundColor", Color("black", 2), isTheme("DARK")],
      ["backgroundColor", Color("white", 0, 0.2), isTheme("LIGHT")],
      ["padding", "20px"],
      ["borderRadius", "10px"],
      ["width", "100%"],
      ["height", "100%"],
      ["justifyContent", "center"],
      ["alignItems", "center"],
      ["display", "flex"],
      ["flexDirection", "column"],
      ["cursor", "pointer"],
      ["position", "absolute"],
      ["boxSizing", "border-box"]
    ],
    wrapper_content: [
      ["width", "100%"],
      ["height", "100%"],
      ["overflow", "auto"],
      ["justifyContent", "flex-start"],
      ["position", "relative"],
      ["display", "flex"],
      ["flexDirection", "column"]
    ],
    wrapper_highlight: [
      ["backgroundColor", Color("black", 3), isTheme("DARK")],
      ["backgroundColor", Color("white", 3), isTheme("LIGHT")],
      ["transition", "all 0.2s"]
    ]
  });
  var AddForm = (onupdate) => {
    const viewState = Signal("ADD_BTN", "FORM", "PROCESSING");
    const formModel = Model({ url: "" });
    let controllerState = "IDLE";
    const html7 = HTML({
      attr: Attr,
      css: ClassList,
      css_on_hover: ClassListOnHover,
      on_click: OnClick,
      on_input: OnTextInput,
      set_value_on_model_change: formModel.value,
      set_attr_on_model_change: formModel.attr,
      set_css_on_model_change: formModel.css,
      set_html_on_viewstate_change: viewState.innerHtml,
      style: Style,
      value: Value
    });
    const controller = async (action) => {
      switch (controllerState) {
        case "IDLE":
          switch (action.type) {
            case "ADD":
              controllerState = "ADDING";
              viewState.pub("PROCESSING");
              await addFeed(action.url);
              setTimeout(() => {
                viewState.pub("ADD_BTN");
                onupdate();
              }, 2e3);
          }
        case "ADDING":
          switch (action.type) {
            case "CANCEL":
              viewState.pub("ADD_BTN");
              controllerState = "IDLE";
              break;
          }
          break;
      }
    };
    const Processing = () => {
      const wrapper2 = html7("div", ["css", css3("wrapper_content")]);
      const processing = html7("div", ["css", css3("processing")]);
      return wrapper2(processing(LoadingAnimation("Processing")));
    };
    const AddBtn = () => {
      const wrapper2 = html7("div", ["css", css3("wrapper_content")], ["on_click", () => viewState.pub("FORM")]);
      const btn = html7("div", ["css", css3("add_feed_button")], ["css_on_hover", css3("add_feed_button", "add_feed_button_highlight")]);
      const plus = html7("div", ["css", css3("add_feed_text")], ["style", "font-size:50px;"]);
      const txt = html7("div", ["css", css3("add_feed_text")]);
      return wrapper2(btn(plus("+"), txt("Add Feed")));
    };
    const Form = () => {
      const wrapper2 = html7("div", ["css", css3("wrapper_content")]);
      const cancel_button = html7("div", ["css", css3("cancel_button")], ["css_on_hover", css3("cancel_button", "cancel_button_highlight")], ["on_click", () => controller({ type: "CANCEL" })]);
      const label = html7("div", ["css", css3("form_label")]);
      const form = html7("form", ["css", css3("form")]);
      const input = html7("input", ["css", css3("form_input")], ["on_input", (val) => formModel.set({ url: val })], ["set_value_on_model_change", "url"], ["value", formModel.get("url")]);
      const add_button = html7("button", ["attr", "disabled", "true"], ["css", css3("form_button", "form_button_disabled")], ["css_on_hover", css3("form_button", "form_button_highlight")], ["on_click", () => controller({ type: "ADD", url: formModel.get("url") })], ["set_attr_on_model_change", "url", "disabled", () => formModel.get("url").length > 0 ? null : "true"], [
        "set_css_on_model_change",
        "url",
        () => formModel.get("url").length > 0 ? css3("form_button") : css3("form_button", "form_button_disabled")
      ]);
      const feed_link = ({ title, url }) => html7("div", ["css", css3("feed")], ["css_on_hover", css3("feed", "feed_highlight")], ["on_click", () => formModel.set({ url })])(title);
      return wrapper2(cancel_button("cancel"), label("Add A Feed"), form(input(), add_button("+")), html7("div", ["css", css3("form_label")])("Or Pick One From Below"), html7("div", ["css", css3("feeds")])(...getFeedsNotStored().map(feed_link)));
    };
    const wrapper = html7("div", ["css", css3("wrapper")], ["set_html_on_viewstate_change", "ADD_BTN", () => AddBtn()], ["set_html_on_viewstate_change", "FORM", () => Form()], ["set_html_on_viewstate_change", "PROCESSING", () => Processing()]);
    return html7("div", ["css", css3("container")])(wrapper(AddBtn()));
  };

  // client/core/articles.ts
  var StorageKey = "articles_read";
  var getArticlesRead = () => {
    const stored = window.localStorage.getItem(StorageKey) ?? "[]";
    const parsed = JSON.parse(stored);
    return parsed;
  };
  var setArticleAsRead = (title) => {
    const articlesRead = getArticlesRead();
    articlesRead.push(title);
    window.localStorage.setItem(StorageKey, JSON.stringify(articlesRead));
  };

  // client/app/features/Feed/Articles.ts
  var css4 = CSS({
    delete: [
      ["fontSize", "8px"],
      ["textAlign", "center"],
      ["color", Color("white", 0, 0.7), isTheme("DARK")],
      ["color", Color("black", 0, 0.7), isTheme("LIGHT")],
      ["position", "absolute"],
      ["top", 0],
      ["right", 0],
      ["cursor", "pointer"],
      ["fontFamily", "arial"],
      ["backgroundColor", Color("black", 0, 1), isTheme("DARK")],
      ["backgroundColor", Color("white", 0, 1), isTheme("LIGHT")],
      ["borderRadius", "100%"],
      ["width", "25px"],
      ["height", "25px"],
      ["display", "flex"],
      ["alignItems", "center"],
      ["justifyContent", "center"],
      ["transition", "all 0.3s"]
    ],
    delete_hover: [
      ["backgroundColor", Color("white"), isTheme("DARK")],
      ["backgroundColor", Color("black"), isTheme("LIGHT")],
      ["color", Color("black"), isTheme("DARK")],
      ["color", Color("white"), isTheme("LIGHT")],
      ["transition", "all 0.3s"]
    ],
    feedUrl: [
      ["fontSize", "11px"],
      ["textAlign", "center"],
      ["textTransform", "uppercase"],
      ["color", Color("white", 0, 0.1), isTheme("DARK")],
      ["color", Color("black", 0, 0.2), isTheme("LIGHT")],
      ["marginBottom", "10px"],
      ["wordBreak", "break-all"]
    ],
    link: [
      ["display", "block"],
      ["textAlign", "center"],
      ["padding", "5px"],
      ["fontSize", "12px"],
      ["padding", "5px"],
      ["transition", "all 0.5s"],
      ["borderRadius", "3px"]
    ],
    link_unread: [
      ["color", Color("white", 0, 0.5), isTheme("DARK")],
      ["color", Color("black", 0, 0.5), isTheme("LIGHT")],
      ["textDecoration", "none"]
    ],
    link_read: [
      ["textDecoration", "line-through"],
      ["color", Color("white", 0, 0.1), isTheme("DARK")],
      ["color", Color("black", 0, 0.1), isTheme("LIGHT")]
    ],
    link_hover: [
      ["color", Color("black"), isTheme("DARK")],
      ["backgroundColor", Color("yellow"), isTheme("DARK")],
      ["backgroundColor", Color("red", 0, 0.4), isTheme("LIGHT")]
    ],
    title: [
      ["fontSize", "15px"],
      ["textAlign", "center"],
      ["marginBottom", "5px"],
      ["textIndent", "5px"],
      ["padding", "10px 0 5px"],
      ["color", Color("white", 0, 0.7), isTheme("DARK")],
      ["color", Color("black", 0, 0.7), isTheme("LIGHT")]
    ]
  });
  var ArticleSignal = Signal("UPDATED");
  var html2 = HTML({
    css: ClassList,
    css_on_hover: ClassListOnHover,
    attr: Attr,
    on_click: OnClick,
    on_signal: ArticleSignal.innerHtml,
    style: InlineStyles
  });
  var Articles = ({ title, url, articles }, ondelete) => {
    const ArticleList = () => {
      const wrapper2 = html2("div", [
        "style",
        ["paddingLeft", "20px"],
        ["paddingRight", "20px"],
        ["marginBottom", "20px"],
        ["height", "300px"],
        ["overflow", "auto"]
      ]);
      return wrapper2(...articles.map(({ title: title2, link }) => html2("a", ["css", getArticlesRead().includes(title2) ? css4("link", "link_read") : css4("link", "link_unread")], ["css_on_hover", css4("link", "link_hover")], ["attr", "href", link], ["attr", "target", "_blank"], ["on_click", () => (setArticleAsRead(title2), ArticleSignal.pub("UPDATED"))])(title2)));
    };
    const wrapper = html2("div", ["style", ["width", "100%"], ["position", "relative"]]);
    const title_wrapper = html2("div", ["css", css4("title")]);
    const delete_button = html2("div", ["css", css4("delete")], ["on_click", ondelete], ["css_on_hover", css4("delete", "delete_hover")]);
    const feed_url = html2("div", ["css", css4("feedUrl")]);
    const list = html2("div", ["on_signal", "UPDATED", ArticleList]);
    return wrapper(title_wrapper(title), delete_button("X"), feed_url(url), list(ArticleList()));
  };

  // client/app/features/Header.ts
  var css5 = CSS({
    credit: [
      ["color", Color("white", 0, 0.2), isTheme("DARK")],
      ["color", Color("black", 0, 0.3), isTheme("LIGHT")],
      ["margin", "0 10px"]
    ],
    link: [
      ["color", Color("white", 0, 0.5), isTheme("DARK")],
      ["color", Color("black", 0, 0.7), isTheme("LIGHT")],
      ["textDecoration", "none"],
      ["fontSize", "11px"],
      ["letterSpacing", "2px"]
    ],
    menu: [
      ["display", "flex"],
      ["flexDirection", "row"],
      ["alignItems", "center"],
      ["margin", "0 0 0 10px"]
    ]
  });
  var html3 = HTML({
    css: ClassList,
    style: Style,
    attr: Attr
  });
  var Menu = () => {
    const menu = html3("div", ["css", css5("menu")]);
    const credit = html3("div", ["css", css5("credit")]);
    const owl = html3("div", ["style", "font-size:20px;margin-right:10px;"]);
    const link = html3("a", ["css", css5("link")], ["attr", "href", "http://github.com/kvnlnt/feedme"], ["attr", "target", "_blank"]);
    return menu(credit("Made With Feds"), owl("\u{1F989}"), link("GITHUB"));
  };

  // client/app/components/Logo.ts
  var css6 = CSS({
    bg: [
      ["cursor", "pointer"],
      ["backgroundColor", Color("white"), isTheme("DARK")],
      ["backgroundColor", Color("black", 0, 0.3), isTheme("LIGHT")],
      ["color", Color("black"), isTheme("DARK")],
      ["color", Color("white"), isTheme("LIGHT")],
      ["padding", "5px 30px"],
      ["borderRadius", "3px"],
      ["transition", "all 0.25s"]
    ],
    bg_onhover: [
      ["backgroundColor", Color("yellow"), isTheme("DARK")],
      ["backgroundColor", Color("red", 0, 0.4), isTheme("LIGHT")],
      ["color", Color("black"), isTheme("DARK")],
      ["color", Color("white"), isTheme("LIGHT")]
    ],
    text: [
      ["fontFamily", "monospace"],
      ["fontSize", "25px"],
      ["fontWeight", "900"]
    ]
  });
  var html4 = HTML({
    css: ClassList,
    css_on_hover: OnHoverClassList,
    onclick: OnClick
  });
  var Logo = ({ onClick }) => {
    const bg = html4("div", ["css", css6("bg")], ["onclick", onClick], ["css_on_hover", css6("bg", "bg_onhover")]);
    const txt = html4("div", ["css", css6("text")]);
    return bg(txt("FEEDME"));
  };

  // client/app/pages/News.ts
  var css7 = CSS({
    feed: [
      ["backgroundColor", Color("black", 2), isTheme("DARK")],
      ["backgroundColor", Color("white", 0, 0.2), isTheme("LIGHT")],
      ["padding", "10px"],
      ["borderRadius", "10px"],
      ["minWidth", "300px"],
      ["minHeight", "300px"],
      ["justifyContent", "center"],
      ["alignItems", "center"],
      ["display", "flex"]
    ],
    feeds: [
      ["minHeight", "100vh"],
      ["fontFamily", "monospace"],
      ["maxWidth", "1800px"],
      ["display", "flex"],
      ["justifyContent", "center"],
      ["alignItems", "flex-start"],
      ["margin", "2vh 0"],
      ["padding", "0 0 0 0"],
      ["transform", "skewY(-3deg)"],
      ["flex", 1],
      ["height", "100vh"],
      ["overflowY", "auto"],
      ["overflowX", "hidden"]
    ],
    grid: [
      ["display", "grid"],
      ["gridGap", "20px"],
      ["padding", "25px"],
      ["boxSizing", "border-box"],
      ["color", Color("white")],
      ["paddingBottom", "100px"]
    ],
    one_col: [["gridTemplateColumns", "1fr"]],
    row: [
      ["gridColumn", "1/-1"],
      ["display", "flex"],
      ["alignItems", "center"],
      ["justifyContent", "space-between"],
      ["padding", "0 20px"],
      ["position", "relative"]
    ],
    three_cols: [["gridTemplateColumns", "repeat(3, 1fr)"]],
    ui: [
      ["display", "flex"],
      ["flexDirection", "row"],
      ["width", "100vw"],
      ["justifyContent", "center"],
      ["backgroundColor", Color("black"), isTheme("DARK")],
      ["backgroundColor", Color("white", 0, 0.8), isTheme("LIGHT")]
    ]
  });
  var NewsViewState = Signal("UPDATED");
  var html5 = HTML({
    css: ClassList,
    style: Style,
    attr: Attr,
    on_view_change: NewsViewState.innerHtml,
    on_resize: OnResize,
    on_load: OnLoad
  });
  var News = () => {
    const handleGridSize = (el) => {
      const width = window.innerWidth;
      if (width > 0 && width < 1100)
        el.className = css7("grid", "one_col");
      if (width >= 1100)
        el.className = css7("grid", "three_cols");
    };
    const getNews = () => {
      const feedList = getFeeds();
      const gridType = css7("grid", window.innerWidth < 1100 ? "one_col" : "three_cols");
      const grid = html5("div", ["on_resize", handleGridSize], ["on_load", handleGridSize], ["css", gridType]);
      const row = html5("div", ["css", css7("row")]);
      const articles = html5("div", ["css", css7("feed")]);
      return grid(row(ThemePicker(), Logo({ onClick: () => null }), Menu()), ...feedList.map((feed, i) => articles(Articles(feed, () => (removeFeed(i), NewsViewState.pub("UPDATED"))))), AddForm(() => NewsViewState.pub("UPDATED")));
    };
    const ui = html5("div", ["css", css7("ui")]);
    const feeds = html5("div", ["css", css7("feeds")], ["on_view_change", "UPDATED", getNews]);
    return ui(feeds(getNews()));
  };

  // client/main.ts
  var html6 = HTML({});
  window.addEventListener("DOMContentLoaded", async () => {
    const getStyles = () => `
    html,body {
      background-color: rgb(13, 13, 13);
      padding: 0px;
      margin: 0px;
      display: flex;
      justify-content: center;
      align-items: flex-start;
      width: 100%;
      height: 100%;
      overflow:hidden;
    }
    *::-webkit-scrollbar { width: 12px; }
    *::-webkit-scrollbar-thumb {
      border-radius: 20px; 
      border: 1px solid ${Color("white", 0, theme.get("state") === "DARK" ? 0.1 : 0.6)}; 
    }
  `;
    const globalStyles = html6("style")(getStyles());
    theme.sub("state", (model) => globalStyles.innerHTML = getStyles());
    document.head.appendChild(globalStyles);
    document.body.appendChild(News());
  });
})();
//# sourceMappingURL=app.js.map
