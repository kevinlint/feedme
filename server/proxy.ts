import { serve } from 'https://deno.land/std@0.132.0/http/server.ts';

async function handler(req: Request): Promise<Response> {
  const url = new URL(req.url);
  const q = url.searchParams.get('q');
  if (!q) return new Response('No Resource');
  const res = await fetch(q);
  const html = await res.text();
  return new Response(html, {
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  });
}

serve(handler, { port: 4242 });
console.log('🛰 PROXY: http://localhost:4242');
